package edu.mondragon.webeng1.mvc_exercise.filter;

import java.io.IOException;
import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import edu.mondragon.webeng1.mvc_exercise.domain.news.model.NewsItem;
import edu.mondragon.webeng1.mvc_exercise.domain.user.model.User;
import edu.mondragon.webeng1.mvc_exercise.domain.news.dao.NewsItemFacade;
import edu.mondragon.webeng1.mvc_exercise.helper.ControllerHelper;

@WebFilter("/news/*")
public class NewsItemFilter implements Filter {
    public NewsItemFilter() {}

    public void destroy() {}

    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
            throws IOException, ServletException {
        System.out.println("NewsItem Filter");
        HttpServletRequest request = (HttpServletRequest) req;
        request.setCharacterEncoding("UTF-8");
        HttpServletResponse response = (HttpServletResponse) res;
        HttpSession session = request.getSession(true);

        ControllerHelper helper = new ControllerHelper(request);
        String action = helper.getAction();
        int code;

        switch (action) {
            case "create":
                code = filterCreation(session);
                break;
            case "edit":
            case "delete":
                code = filterModification(helper, session, request);
                break;
            case "view":
            case "list":
                System.out.println("Even if no user in session, view and list permited: " + action);
                code = HttpServletResponse.SC_OK;
                break;
            default:
                System.out.println("Unknown News Item action: " + action);
                session.setAttribute("error", "error.400.unknown_action");
                code = HttpServletResponse.SC_BAD_REQUEST;
        }

        if (code != HttpServletResponse.SC_OK) {
            // Guard clause
            session.setAttribute("errorCode", code);
            RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/view/error.jsp");
            response.setStatus(code);
            dispatcher.forward(request, response);
            return;
        }
        
        chain.doFilter(req, res);
    }

    public void init(FilterConfig fConfig) throws ServletException {
        System.out.println("Init NewsItem filter.");
    }

    private int filterCreation(HttpSession session) {
        User user = (User) session.getAttribute("user");

        if (user == null) {
            // Guard clause
            System.out.println("News Item creation needs a user in session.");
            session.setAttribute("error", "error.403.not_session_user");
            return HttpServletResponse.SC_FORBIDDEN;
        }

        System.out.println("News Item creation allowed for users: " + user.getUserId());
        return HttpServletResponse.SC_OK;
    }

    private int filterModification(ControllerHelper helper, HttpSession session, HttpServletRequest request) {
        User user = (User) session.getAttribute("user");
        int newsItemId = helper.getId();
        NewsItemFacade nif = new NewsItemFacade();
        NewsItem newsItem = nif.loadNewsItem(newsItemId);

        if (newsItem.getNewsItemId() < 0) {
            // Guard clause
            System.out.println("News item does not exist: "+ newsItemId);
            session.setAttribute("error", "error.404.news_item_not_found");
            return HttpServletResponse.SC_NOT_FOUND;
        }
        
        if (user == null || user.getUserId() != newsItem.getAuthor().getUserId()){
            // Guard clause
            System.out.println("Session user is not the author.");
            session.setAttribute("error", "error.403.user_not_author");
            return HttpServletResponse.SC_FORBIDDEN;
        }

        System.out.println("Session user is the author.");
        request.setAttribute("newsItem", newsItem);
        return HttpServletResponse.SC_OK;
    }
}
