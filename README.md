# MVC Exercise 3

In this exercise we have added a **controller**, a **filter**, **DAO & facade classes** and different **views** for making a **CRUD** (Create/Read/Update/Delete) for **NewsItems**.

## Explainations

We can see that NewsItem JavaBean has date as a **java.util.Date**, lang as a **java.util.Locale** and author as a **User**, but in the database they are timestamp, String and **int**.

```java
// NewsItem.java
private Date date;
private Locale lang;
private User author;
```

```sql
/* mvc_exercise.sql (at 10-dao-login exercise)*/
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lang` varchar(45) DEFAULT NULL,
  `authorId` int(11) NOT NULL,
```

How do we transform them? They do not have to appear in the creation/edition forms:

* Date will be the current date when creating the NewsItem.
* The autor with be the current user in session.
* Locale will be the current locale when creating the NewsItem.

### NewsItem date

In MySQL it is configured as "CURRENT_TIMESTAMP" as the default value for the date, so we dont have to add it in the insert.

```sql
/* mvc_exercise.sql (at 10-dao-login exercise)*/
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
```

In order to load that date from the database, we can do it like this:

```java
// DaoNewsItemMySQL.java
...
    Timestamp ts = rs.getTimestamp("date");
    Date date = new Date(ts.getTime());
    newsItem.setDate(date);
...
```

Now we can work with regular java dates quite confortably.

### News Item author

In order to insert the author in the database, we will store only the Id:

```java
// DaoNewsItemMySQL.java
...
String sqlInsert = "INSERT INTO news_item (title,body,lang,authorId) VALUES(?,?,?,?)";
...
stm.setInt(4, newsItem.getAuthor().getUserId());
...
```

In order to load the user from the database, we will get the authorId, and then, we can use ```DaoUserMySQL```.

```java
// DaoNewsItemMySQL.java
...
int authorId = rs.getInt("authorId");
User author = new DaoUserMySQL().loadUser(authorId);
newsItem.setAuthor(author);
...
```

### How to get current locale in a controller

We will start creating a defaultLocale, just in case everything else fails.

Then we will get the locale from FMT and browsers locale.

```java
// NewsItemController.java
...
private Locale getLocale(HttpServletRequest request, HttpSession session) {
    Locale defaultLocale = Locale.forLanguageTag("en-UK"); // Default locale.
    Locale fmtLocale = (Locale) Config.get(session, Config.FMT_LOCALE); // Locale from FMT library
    Locale browserLocale = request.getLocale(); // Browser locale

    if (fmtLocale == null && browserLocale == null)
        return defaultLocale;
    
    if (fmtLocale == null)
        return browserLocale;

    return fmtLocale;
}
```

If FMTlocale is set, that locale will be returned. Otherwise, if browserLocale is set, that locale will be returned. If none of them is defined, defaultLocale will be returned.

That can be used to set the locale of the NewsItem when it is created.

```java
// DaoNewsItemMySQL.java
...
String sqlInsert = "INSERT INTO news_item (title,body,lang,authorId) VALUES(?,?,?,?)";
...
stm.setString(3, newsItem.getLang().getLanguage());
...
```

In order to load just the newsItem in a locale:

```java
// DaoNewsItemMySQL.java
...
String sqlQuery = "SELECT * FROM news_item WHERE lang=?";
String languageTag = locale.getLanguage();
...
try {
    stm = connection.prepareStatement(sqlQuery);
    stm.setString(1, languageTag);
    rs = stm.executeQuery();
    ...
```

In order to show the language of a newsItem in a JSP file:

```jsp
<!-- news_item.jsp -->
${requestScope.newsItem.lang.language}
```

As we use it with FMT messages:

```jsp
<!-- news_item.jsp -->
<i><fmt:message key="language.${requestScope.newsItem.lang.language}"/></i>
```
